# README #

This is BETTA, Why its called BETA ?
Because it is betta then nothing.

### Repository info ###

* Ignite Smart Bank
* Version : 0.6874167
* https://kostja_t_77@bitbucket.org/kostja_t_77/ignite.git

### To setup the project use ###

* Maven - will migrate to "Gradle" in future
* Bower - JS packages install
* Flyway - DB

### Project description ###

This is beta version : This means that this "SmartBANK" not done.
There is no any tests(not committed).
Code is not fine (need to refactor and review).
No JavaDoc.
There are some unused code and not finished pages.

But all basic functionality is working.

### What about technologies (for now) ###

* bitbucket - YES
* java 8 - NO (Java 7)
* Tomcat 8 - NO (Jetty)
* Postgres - YES
* bootstrap/jsp/angularjs - YES
* Spring 4 - YES
* Hibernate 4.3 - YES
* Spring MVC - YES
* flyway - YES
* gradle 2 - NO (Maven)
* Junit, Mockito, MockMvc - NO (not committed)
* Spring Security - YES