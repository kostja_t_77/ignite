var services = angular.module('smartBankApp.services', []);

services.factory('UserService', function($http, $q, $rootScope) {
    
    return {
    	authenticate: function(username, password){
    		var req = {
    				method: 'POST',
   				 	url: 'smart_bank/public/authenticate',
   				 	headers: {'Content-Type': undefined },
   				 	params : {'username' : username , 'password' : password }
   			}
    		return $http(req);
        },
        
        getLoggedInUser: function(){
        	return $http.get('smart_bank/common/getLoggedInUser');
        },
        
        logOut: function(){
        	return $http.get('smart_bank/common/logOut');
        }
    };
    
	
});