function AccountListController($scope, $http, $location, $rootScope) {

	$scope.selectedAccounts = [];
	
    $scope.accountOptions = {
    		data: 'accountData',
    		totalServerItems:'totalAccountItems',
    		showFooter: true,
    		multiSelect: false,
    		selectedItems: $scope.selectedAccounts,
    		columnDefs: [
    		             {field:'account', displayName: 'Account'},
    		             {field:'balance', displayName: 'Balance'},
    		             {field:'currency', displayName:'Currency'},      
    		             ],
    		afterSelectionChange: function(data) {
    			console.log( "selected account = ", $scope.selectedAccounts[0].account );
    			$location.path("/transactionList/" + $scope.selectedAccounts[0].account);
    		}
    };
    
    $scope.searchAccounts = function(){
    	console.log('searchAccounts');
		$scope.accountData = $http.post('smart_bank/common/getAccountList/', $rootScope.bankUser.id ).success(function(accountData) {
			console.log('searchAccounts, userId = ' + $rootScope.bankUser.id );
			$scope.accountData = accountData;
			$scope.totalAccountItems = accountData.length;
	   });
	}
	
	$scope.searchAccounts();
    
	$scope.getInspectionTableStyle = function() {
  	   var rowHeight=30;
  	   var headerHeight=45;
  	   var footerHeight=55;
  	   var renderedRows = $scope.accountOptions.$gridScope.renderedRows.length;
  	   console.log("Rendered table rows = " + renderedRows);
  	   return {
  		   height: (renderedRows * rowHeight + headerHeight + footerHeight) + "px"
  	   };
    };
	
}