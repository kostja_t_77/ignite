function TransactionListController($scope, $http, $location, $rootScope, $routeParams) {

	$scope.transactionFilter = null;
	$scope.selectedTransactions = [];
    $scope.filterDates = {};
    
    $scope.pagingOptions = {
            pageSizes: [2, 5, 10, 20],
            pageSize: 2,
            currentPage: 1
    };
	
    $scope.transactionOptions = {
    		data: 'transactionData',
    		totalServerItems:'totalTransactionItems',
    		enablePaging: true,
    		pagingOptions: $scope.pagingOptions,
    		showFooter: true,
    		multiSelect: false,
    		selectedItems: $scope.selectedTransactions,
    		columnDefs: [
    		             {field:'accountTo', displayName: 'Account to'},
    		             {field:'amount', displayName: 'Amount'},
    		             {field:'balance', displayName: 'Balance'},
    		             {field:'recipient', displayName:'Recipient name'},
    		             {field:'dateAdded', displayName:'Date', cellTemplate: '<div style="text-align:center;">{{row.getProperty(col.field) | date: "dd/MM/yyyy HH:mm" }}</div>'},
    		             ],
    		afterSelectionChange: function(data) {
    		    console.log( "selected transaction id = ", $scope.selectedTransactions[0].id );
    		    $location.path("/transactionView/" + $scope.selectedTransactions[0].id);
    	}
    }
    
	$scope.getDefaultTransactionFilter = function(){
		var filter={};
		if($routeParams.account){
			filter.account = $routeParams.account;
			console.log( "passed account = " + account);
		}
		filter.pageSize = $scope.pagingOptions.pageSize;
		filter.page = 1;
		filter.startDate = moment().startOf("day").subtract(30, "day")
		filter.endDate = moment().endOf("day");
		filter.comment = null;
	    return filter;
	}
    
    $scope.getTransactionData = function(pageSize, page){
    	
		$scope.transactionFilter.pageSize = pageSize;
		$scope.transactionFilter.page = page;
		$scope.transactionFilter.startDate = $scope.filterDates.startDate;
		$scope.transactionFilter.endDate = $scope.filterDates.endDate;
		
    	console.log('getPagedDataAsync page = ' + $scope.transactionFilter.page + ' pageSize = ' + $scope.transactionFilter.pageSize + ' account = ' + $scope.transactionFilter.account );
    	
    	$http.post('smart_bank/common/searchTransactionsListSize', $scope.transactionFilter ).success(function(totalTransactionItems) {
    		$scope.totalTransactionItems = totalTransactionItems;
    	});
    	
		$http.post('smart_bank/common/searchTransactions/', $scope.transactionFilter ).success(function(transactionData) {
			$scope.transactionData = transactionData;
		});
	}
    
    $scope.searchTransactions = function(){
    	$scope.getTransactionData($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }

    $scope.init = function() {
		$http.post('smart_bank/common/getAccountList/', $rootScope.bankUser.id ).success(function(accountList) {
			$scope.accountList = accountList;
		});

		if($scope.transactionFilter == null){
			$scope.transactionFilter = $scope.getDefaultTransactionFilter();
			$scope.filterDates.startDate = $scope.transactionFilter.startDate;
			$scope.filterDates.endDate = $scope.transactionFilter.endDate;
		}
		$scope.searchTransactions();
    }
    
	$scope.init();
	
	$scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
        	if(newVal.pageSize !== oldVal.pageSize){
        		$scope.pagingOptions.currentPage = 1;
        	}
        	$scope.getTransactionData($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    }, true);

	$scope.getInspectionTableStyle = function() {
  	   var rowHeight=30;
  	   var headerHeight=45;
  	   var footerHeight=55;
  	   var renderedRows = $scope.transactionOptions.$gridScope.renderedRows.length;
  	   console.log("Rendered table rows = " + renderedRows);
  	   return {
  		   height: (renderedRows * rowHeight + headerHeight + footerHeight) + "px"
  	   };
    }
    
}