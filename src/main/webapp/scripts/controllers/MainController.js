function MainController($scope, $http, $location, $rootScope, UserService) {
	
	$scope.common = {
		footer: "views/common/footer.html",
		header: "views/common/header.html"
	}
	
    $scope.isLogedIn = function () {
    	var loginned = $rootScope.bankUser != null && $rootScope.bankUser.id > 0;
    	return loginned;
    }
	
    $scope.isActive = function (viewLocation) {
    	$scope.isLogedIn();
    	return $location.path().indexOf(viewLocation) >-1;
    }
	
    $scope.logOut = function () {
    	delete $rootScope.bankUser;
    	delete $rootScope.authToken;
    	UserService.logOut();
    	console.log('logOut');
    	// for path see header.html
    }
	
}