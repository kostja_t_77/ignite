function TransactionViewController($scope, $http, $location, $routeParams, $rootScope) {

	$scope.transaction = {};
	
	$scope.init = function() {
		
		
		$http.post('smart_bank/common/getAccountList/', $rootScope.bankUser.id ).success(function(accountList) {
			$scope.accountList = accountList;
		});
		
		if($routeParams.id != null && $routeParams.id > 0){
			$http.post('smart_bank/common/getTransaction', $routeParams.id).success(function(transaction) {
				console.log('getTransaction, transactionId = ' +  $routeParams.transactionId);
				$scope.transaction = transaction;
		    });
		}
	}

	$scope.init();
	
	
	$scope.submitTransaction = function() {
		$http.post('smart_bank/common/processTransaction', $scope.transaction).success(function(transaction) {
			console.log('processTransaction, ammount = ' +  $scope.transaction.amount);
			$location.path("/accountList");
		});

	}

}