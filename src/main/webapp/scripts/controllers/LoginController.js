function LoginController($scope, $http, $location, $rootScope, UserService) {
	
	$scope.login = function(username, password) {
		
		UserService.authenticate(username, password).success(function() {
			UserService.getLoggedInUser().success(function(user) {
				$rootScope.bankUser = user;
				console.log('login success, user = ' + user.login);
				$location.path("/accountList");
			});
		});
	}

}