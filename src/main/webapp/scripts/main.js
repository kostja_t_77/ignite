var app = angular.module('smartBankApp', [
	'ngBootstrap',
	'ngGrid',
	'ngRoute',
	'fiestah.money',
	'smartBankApp.services'
	]
);

app.config( ['$routeProvider', '$httpProvider', function($routeProvider,$httpProvider) {
	$routeProvider.when("/accountList", {
		templateUrl : "views/accountList.html",
		controller : 'AccountListController'
	}).when("/userList", {
		templateUrl : "views/userList.html",
		controller : 'UserListController'
	}).when("/transactionCompose", {
		templateUrl : "views/transactionCompose.html",
		controller : 'TransactionViewController'
	}).when("/transactionList/:account", {
		templateUrl : "views/transactionList.html",
		controller : 'TransactionListController'
	}).when("/transactionList", {
		templateUrl : "views/transactionList.html",
		controller : 'TransactionListController'
	}).when("/transactionView/:id", {
		templateUrl : "views/transactionView.html",
		controller : 'TransactionViewController'
	}).when("/login", {
		templateUrl : "views/login.html",
		controller : 'LoginController'
	}).otherwise({
		redirectTo : '/login'
	});
	
	/* Register error provider that shows message on failed requests or redirects to login page on
	 * unauthenticated requests */
    $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
        return {
        	'responseError': function(rejection) {
        		var status = rejection.status;
        		var config = rejection.config;
        		var method = config.method;
        		var url = config.url;
      
        		if (status == 401) {
        			$location.path( "/login" );
        		} if (status == 403) {//TODO access denied implement nice error message
        			$location.path( "/login" );
        		}else {
        			$rootScope.error = method + " on " + url + " failed with status " + status;
        		}
              
        		return $q.reject(rejection);
	        	}
	        };
	    }
	);

	
}]);