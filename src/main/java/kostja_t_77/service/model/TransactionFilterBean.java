package kostja_t_77.service.model;

import java.io.Serializable;
import java.util.Date;

public class TransactionFilterBean implements Serializable {

	private static final long serialVersionUID = -4845082146719691649L;

	private String account;
	private Date startDate;
	private Date endDate;
	private String comment;
	private int pageSize;
	private int page;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	@Override
	public String toString() {
		return "account = " + account + " comment = " + comment + " page = " + page + " ageSize = " + pageSize;
	}

}