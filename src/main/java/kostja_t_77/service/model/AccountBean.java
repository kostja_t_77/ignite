package kostja_t_77.service.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountBean implements Serializable{

	private static final long serialVersionUID = -2719535355026728494L;
	
	private String account;
	private String currency;
	private BigDecimal balance;
	
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
}