package kostja_t_77.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TransactionBean implements Serializable {

	private static final long serialVersionUID = 1345363449213691577L;
	
	private Long id;
	private BigDecimal amount;
	private BigDecimal balance;
	private AccountBean accountFrom;
	private String accountTo;
	private String recipient;
	private String description;
	private Date dateAdded;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public AccountBean getAccountFrom() {
		return accountFrom;
	}

	public void setAccountFrom(AccountBean accountFrom) {
		this.accountFrom = accountFrom;
	}

	public String getAccountTo() {
		return accountTo;
	}

	public void setAccountTo(String accountTo) {
		this.accountTo = accountTo;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}
}