package kostja_t_77.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import kostja_t_77.db.dao.AccountDAO;
import kostja_t_77.db.dao.BankUserDAO;
import kostja_t_77.db.dao.TransactionDAO;
import kostja_t_77.db.model.Account;
import kostja_t_77.db.model.BankUser;
import kostja_t_77.db.model.Transaction;
import kostja_t_77.service.SmartBankService;
import kostja_t_77.service.model.AccountBean;
import kostja_t_77.service.model.BankUserBean;
import kostja_t_77.service.model.TransactionBean;
import kostja_t_77.service.model.TransactionFilterBean;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value = "smartBankService")
@Transactional
public class SmartBankServiceImpl implements SmartBankService , UserDetailsService{

	@Resource(name = "bankUserDAO")
	private BankUserDAO bankUserDAO;
	
	@Resource(name = "transactionDAO")
	private TransactionDAO transactionDAO;
	
	@Resource(name = "accountDAO")
	private AccountDAO accountDAO;
	
	public BankUserBean getLoggeInUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object principal = authentication.getPrincipal();
		if (principal instanceof String && ((String) principal).equals("anonymousUser")) {
			throw new RuntimeException("NO anonymousUser allowed !!!");//TODO process login errors
		}
		String username = ((UserDetails)principal).getUsername();
		return loadUserByUsername(username);
	}

	public List<AccountBean> getAccountList(Long userId) {
		bankUserDAO.getById(userId);
		List<AccountBean> ret = new ArrayList<AccountBean>();
		for (Account item : bankUserDAO.getById(userId).getAccounts()) {
			ret.add(getAccountBeanFromAccount(item));
		}
		return ret;
	}
	
	public List<BankUserBean> getBankUserList() {
		List<BankUserBean> ret = new ArrayList<BankUserBean>();
		for (BankUser item : bankUserDAO.getAll()) {
			ret.add(getBankUserBeanFromBankUser(item));
		}
		return ret;
	}
	
	public BankUserBean loadUserByUsername(String username) {
		BankUser ret = bankUserDAO.loadUserByUsername(username);
		if (null == ret) {
			throw new UsernameNotFoundException("No user with userName = '" + username + "' found");//TODO process login errors
		}
		return getBankUserBeanFromBankUser(ret);
	}
	
	public List<TransactionBean> searchTransactions(TransactionFilterBean filter) {
		List<TransactionBean> ret = new ArrayList<TransactionBean>();
		for (Transaction item :  transactionDAO.searchTransactions(filter)) {
			ret.add(getTransactionBeanFromTransaction(item));
		}
		return ret;
	}
	
	public long searchTransactionsListSize(TransactionFilterBean filter) {
		return transactionDAO.searchTransactionsListSize(filter);
	}

	public TransactionBean getTransaction(Long transactionId) {
		return getTransactionBeanFromTransaction(transactionDAO.getById(transactionId));
	}
	
	public void processTransaction(TransactionBean input){
		
		Account loseMoneyAccount = accountDAO.getById(input.getAccountFrom().getAccount());
		if(loseMoneyAccount != null){
			BigDecimal balance = loseMoneyAccount.getBalance().subtract(input.getAmount());
			BigDecimal amount = input.getAmount().multiply(new BigDecimal(-1));
			processAccountTransaction(loseMoneyAccount, input, balance, amount, input.getAccountFrom().getAccount(), input.getAccountTo());
		}
		
		Account getMoneyAccount = accountDAO.getById(input.getAccountTo());
		if(getMoneyAccount != null){
			BigDecimal balance = getMoneyAccount.getBalance().add(input.getAmount());
			BigDecimal amount = input.getAmount();
			processAccountTransaction(getMoneyAccount, input, balance, amount, input.getAccountTo(), input.getAccountFrom().getAccount());
		}
	}
	
	protected void processAccountTransaction(Account acc, TransactionBean trans, BigDecimal balance, BigDecimal amount, String accountFrom, String accounTo) {
		acc.setBalance(balance);
		accountDAO.saveOrUpdate(acc);
		Transaction transaction = new Transaction();
		populateTransaction(trans, transaction, amount, balance, accountFrom, accounTo);
		transactionDAO.saveOrUpdate(transaction);
	}
	
	protected void populateTransaction(TransactionBean input, Transaction output, BigDecimal ammount, BigDecimal balance, String accountFrom, String accounTo){
		output.setBalance(balance);
		output.setAmount(ammount);
		output.setAccountFrom(accountFrom);
		output.setAccountTo(accounTo);
		output.setRecipient(input.getRecipient());
		output.setDescription(input.getDescription());
		output.setDateAdded(new Date());
	}
	
	protected TransactionBean getTransactionBeanFromTransaction(Transaction input){
		TransactionBean output = new TransactionBean();
		output.setId(input.getId());
		output.setAmount(input.getAmount());
		output.setBalance(input.getBalance());
		output.setAccountFrom(getAccountBeanFromAccount(accountDAO.getById(input.getAccountFrom())));
		output.setAccountTo(input.getAccountTo());
		output.setRecipient(input.getRecipient());
		output.setDescription(input.getDescription());
		output.setDateAdded(input.getDateAdded());
		return output;
	}
	
	protected BankUserBean getBankUserBeanFromBankUser(BankUser input){
		BankUserBean output = new BankUserBean();
		output.setId(input.getId());
		output.setLogin(input.getUserName());
		output.setName(input.getName());
		output.setPassword(input.getPassword());
		output.setRoles(new ArrayList<String>(1));
		output.getRoles().add(input.getUserRole().getRoleName());
		return output;
	}
	
	protected AccountBean getAccountBeanFromAccount(Account input){
		AccountBean output = new AccountBean();
		output.setAccount(input.getAccount());
		output.setBalance(input.getBalance());
		output.setCurrency(input.getCurrency());
		return output;
	}
}