package kostja_t_77.service;

import java.util.List;

import kostja_t_77.service.model.AccountBean;
import kostja_t_77.service.model.BankUserBean;
import kostja_t_77.service.model.TransactionBean;
import kostja_t_77.service.model.TransactionFilterBean;

public interface SmartBankService {

	BankUserBean getLoggeInUser();
	
	List<AccountBean> getAccountList(Long userId);
	
	List<BankUserBean> getBankUserList();
	
	BankUserBean loadUserByUsername(String username);
	
	TransactionBean getTransaction(Long transactionId);
	
	void processTransaction(TransactionBean input);
	 
	List<TransactionBean> searchTransactions(TransactionFilterBean filter);
	
	long searchTransactionsListSize(TransactionFilterBean filter);
	
}
