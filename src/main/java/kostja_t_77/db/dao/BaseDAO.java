package kostja_t_77.db.dao;

import java.io.Serializable;
import java.util.List;

public interface BaseDAO<T, ID extends Serializable> {

	T getById(ID id);
	
	List<T> getAll();

	void saveOrUpdate(T request);
	
	void delete(T entity);
	
	List<T> findByField(String fieldName, Object fieldValue);
	
	Integer getMaxValueFromTheColumn(String fieldValue);
    
}