package kostja_t_77.db.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import kostja_t_77.db.dao.TransactionDAO;
import kostja_t_77.db.model.Transaction;
import kostja_t_77.service.model.TransactionFilterBean;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository(value = "transactionDAO")
public class TransactionDAOImpl extends BaseDAOImpl<Transaction, Long> implements TransactionDAO {

	@SuppressWarnings("unchecked")
	public List<Transaction> searchTransactions(TransactionFilterBean filter){
		Query query = getSession().createQuery(createTransactionHQL(filter, false));
		setTransactionHQLParameters(query, filter);
		query.setFirstResult((filter.getPage()-1) * filter.getPageSize());
		query.setMaxResults(filter.getPageSize());
		return query.list();
	}

	public long searchTransactionsListSize(TransactionFilterBean filter) {
		Query query = getSession().createQuery(createTransactionHQL(filter, true));
		setTransactionHQLParameters(query, filter);
		return ((Long) query.uniqueResult()).intValue();
	}
	
	protected String createTransactionHQL(TransactionFilterBean filter, boolean size){
		String hql = size ? "select count(t) " : "";
		hql += "from Transaction t where accountFrom = :accountFrom ";
		if(filter.getComment() != null && !filter.getComment().isEmpty()){
			hql += " and upper(description) like :description ";
		}
		if(filter.getStartDate() != null && filter.getEndDate() != null){
			hql += " and dateAdded between :startDate and :endDate ";
		}
		hql += size ? "" : " order by dateAdded DESC";
		return hql;
	}
	
	protected void setTransactionHQLParameters(Query query, TransactionFilterBean filter){
		query.setString("accountFrom", filter.getAccount());
		if(filter.getComment() != null && !filter.getComment().isEmpty()){
			query.setString("description", "%" + filter.getComment().trim().toUpperCase() + "%" );
		}
		if(filter.getStartDate() != null && filter.getEndDate() != null){
			query.setParameter("startDate", filter.getStartDate());
			query.setParameter("endDate", filter.getEndDate());
		}
	}
	
}