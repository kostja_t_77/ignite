package kostja_t_77.db.dao.impl;

import kostja_t_77.db.dao.BankUserDAO;
import kostja_t_77.db.model.BankUser;

import org.springframework.stereotype.Repository;

@Repository(value = "bankUserDAO")
public class BankUserDAOImpl extends BaseDAOImpl<BankUser, Long> implements BankUserDAO{

	public BankUser loadUserByUsername(String username) {
		BankUser ret = (BankUser)getSession().createQuery("from BankUser bu where upper(bu.userName) = :userName")
		           .setString("userName", username.toUpperCase())
		           .uniqueResult();
		return ret;
	}

}
