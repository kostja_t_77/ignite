package kostja_t_77.db.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.annotation.Resource;

import kostja_t_77.db.dao.BaseDAO;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class BaseDAOImpl<T, ID extends Serializable>  implements BaseDAO<T, ID>{
	
	private Class<T> persistentClass;
		
    @Resource(name="sessionFactory")
    protected SessionFactory sessionFactory;
    
    protected final Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public void saveOrUpdate(T entity) {
		getSession().saveOrUpdate(entity);
	}
	
	public T getById(ID id) {
		@SuppressWarnings("unchecked")
		T object=(T) getSession().get(getPersistentClass(), id);
		return object;
	}
	
	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		return getSession().createCriteria(getPersistentClass()).list();
	}
	
	public void delete(T entity) {
		getSession().delete(entity);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findByField(final String fieldName, final Object fieldValue) {
		Criteria criteria = getSession().createCriteria(getPersistentClass());
		criteria.add(Restrictions.eq(fieldName, fieldValue));
		return criteria.list();
	}

	public Integer getMaxValueFromTheColumn(String fieldValue){
		return (Integer)getSession().createCriteria(getPersistentClass()).setProjection(Projections.max(fieldValue)).uniqueResult();
	}
	
	
	@SuppressWarnings("unchecked")
	protected Class<T> getPersistentClass() {
		if (this.persistentClass == null) {
			return this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
					.getGenericSuperclass()).getActualTypeArguments()[0];
		} else {
			return this.persistentClass;
		}
	}	
}