package kostja_t_77.db.dao.impl;

import kostja_t_77.db.dao.AccountDAO;
import kostja_t_77.db.model.Account;

import org.springframework.stereotype.Repository;

@Repository(value = "accountDAO")
public class AccountDAOImpl extends BaseDAOImpl<Account, String> implements AccountDAO {

}
