package kostja_t_77.db.dao;

import kostja_t_77.db.model.BankUser;

public interface BankUserDAO extends BaseDAO<BankUser, Long>{
	
	BankUser loadUserByUsername(String username);

}
