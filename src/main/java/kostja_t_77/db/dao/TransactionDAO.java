package kostja_t_77.db.dao;

import java.util.List;

import kostja_t_77.db.model.Transaction;
import kostja_t_77.service.model.TransactionFilterBean;

public interface TransactionDAO extends BaseDAO<Transaction, Long>{

	List<Transaction> searchTransactions(TransactionFilterBean filter);
	
	long searchTransactionsListSize(TransactionFilterBean filter);
	
}
