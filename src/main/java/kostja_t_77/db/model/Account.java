package kostja_t_77.db.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ACCOUNT")
public class Account {
	
	@Id
	@Column(name = "account")
	private String account;
	
	@ManyToOne
	@JoinColumn
	private BankUser user;
	
	@Column(name = "currency")
	private String currency;
	
	@Column(name = "balance")
	private BigDecimal balance;
	
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public BankUser getUser() {
		return user;
	}
	public void setUser(BankUser user) {
		this.user = user;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
}