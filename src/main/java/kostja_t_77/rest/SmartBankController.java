package kostja_t_77.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import kostja_t_77.service.SmartBankService;
import kostja_t_77.service.model.AccountBean;
import kostja_t_77.service.model.BankUserBean;
import kostja_t_77.service.model.TransactionBean;
import kostja_t_77.service.model.TransactionFilterBean;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/smart_bank/common")
public class SmartBankController {
	
	private static final Logger logger = Logger.getLogger(SmartBankController.class);

	@Autowired
	private SmartBankService smartBankService;

	@RequestMapping(value="/getLoggedInUser", method = RequestMethod.GET)
	public @ResponseBody BankUserBean getLoggeInUser() {
		logger.debug("getLoggedInUser");
		return smartBankService.getLoggeInUser();
	}
	
	@RequestMapping(value="/logOut", method = RequestMethod.GET)
	public @ResponseBody void authenticate(HttpServletRequest request){
		logger.debug("logOut");
		SecurityContextHolder.clearContext();
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
		}
	}
	
	@RequestMapping(value="/getAccountList", method = RequestMethod.POST)
	public @ResponseBody
	List<AccountBean> getAccountList(@RequestBody long userId) {
		logger.debug("getAccountList, userId = " + userId);
		return smartBankService.getAccountList(userId);
	}
	
	@RequestMapping(value="/getTransaction", method = RequestMethod.POST)
	public @ResponseBody
	TransactionBean getTransaction(@RequestBody long transactionId) {
		logger.debug("getTransaction, id = " + transactionId);
		return smartBankService.getTransaction(transactionId);
	}
	
	@RequestMapping(value="/processTransaction", method = RequestMethod.POST)
	public @ResponseBody
	void processTransaction(@RequestBody TransactionBean transaction) {
		logger.debug("getTransaction, ammount = " + transaction.getAmount());
		smartBankService.processTransaction(transaction);
	}
	
	@RequestMapping(value="/searchTransactions", method = RequestMethod.POST)
	public @ResponseBody
	List<TransactionBean> searchTransactions(@RequestBody TransactionFilterBean filter) {
		logger.debug("searchTransactions, filter = " + filter);
		return smartBankService.searchTransactions(filter);
	}
	
	@RequestMapping(value="/searchTransactionsListSize", method = RequestMethod.POST)
	public @ResponseBody long searchTransactionsListSize(@RequestBody TransactionFilterBean filter) {
		logger.debug("searchTransactionsListSize, filter = " + filter);
		return smartBankService.searchTransactionsListSize(filter);
	}
	
}