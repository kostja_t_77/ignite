package kostja_t_77.rest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/smart_bank/public")
public class PublicController {
	
	private static final Logger logger = Logger.getLogger(PublicController.class);
	
	@RequestMapping(value="/authenticate", method = RequestMethod.POST)
	public @ResponseBody void authenticate(){
		logger.debug("authenticate");
	}
	
}