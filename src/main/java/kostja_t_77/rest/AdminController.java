package kostja_t_77.rest;

import java.util.List;

import kostja_t_77.service.SmartBankService;
import kostja_t_77.service.model.BankUserBean;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/smart_bank/admin")
public class AdminController {
	
	private static final Logger logger = Logger.getLogger(AdminController.class);

	@Autowired
	private SmartBankService smartBankService;
	
	@RequestMapping(value="/getBankUserList", method = RequestMethod.GET)
	public @ResponseBody
	List<BankUserBean> getBankUserList() {
		logger.debug("getBankUserList");
		return smartBankService.getBankUserList();
	}
	
}
