package kostja_t_77.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.crypto.password.PasswordEncoder;


public class SaltedSHA256PasswordEncoder implements PasswordEncoder {

	private final String salt;
	private final MessageDigest digest;


	public SaltedSHA256PasswordEncoder(String salt) throws NoSuchAlgorithmException {
		this.salt = salt;
		this.digest = MessageDigest.getInstance("SHA-256");
	}

	public String encode(CharSequence rawPassword) {	
		try {
			return new String(Hex.encode(this.digest.digest((rawPassword + this.salt).getBytes("UTF-8"))));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("UTF-8 not supported");
		}
	}

	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		boolean ret = false;
		if(rawPassword != null && encodedPassword != null){
			ret = this.encode(rawPassword).equals(encodedPassword.trim());
		}
		return ret;
	}
}