package kostja_t_77.test;


import kostja_t_77.service.SmartBankService;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/app-config.xml"})
public class SmartBankServiceTest {
	
	private static final Logger logger = Logger.getLogger(SmartBankServiceTest.class);

	@Autowired
	private SmartBankService smartBankService;
	
	@Test //TODO testSomething
	public void testSomething() {
		assertEquals(smartBankService.getAccountList(1L).size(), 3);
        logger.debug(smartBankService.getAccountList(1L).size());
	}

}